// ------------------------------------
// Se establece configuración de entorno
process.env.ENV = 'dev';
process.env.BD = 'mysql'
// ------------------------------------
// ------------------------------------

import UserController from './api/user/user.controller';
import App from './App';
import { AuthService } from './core/Services/AuthService';
import { environment } from './environments/environments';
import { splash } from './shared/utilidades/extras.util';

// Configuración de environment
// Recuperación de configuración de proyecto
const proyecto = environment.getProject();
const version = environment.getVersion();
const acceso = environment.getCert();
const port = environment.getPort();
const bd = environment.db.active;
// Instanciación de auth service
AuthService.getInstance();
AuthService.configureConectionWithKeycloak(require('express')(), acceso);
// Configuración de todos los controlarores
const app = new App([
    new UserController(),
], acceso);
// Se recupera aplicación express
const _api = app.getApp();
// Se pone en escucha el servidor
_api.listen(port, () => console.log(splash(proyecto, version, port, bd)));
