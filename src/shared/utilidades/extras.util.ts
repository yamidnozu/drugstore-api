export const splash = (proyecto: string, version: string, port: number, bd: string) => `
                _______________________________________
                    PROYECTO:     ${proyecto}      
                    VERSION:      ${version}      
                    URL:          <HOST>:${port}  
                    PID:          ${process.pid}  
                    DATABASE:     ${bd}
       `;