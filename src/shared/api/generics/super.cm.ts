import SqlException from '../../../core/exceptions/SqlException';
import { environment } from '../../../environments/environments';
import { QSuper } from './super.query';
import { MySqlAdapter } from '../../../core/database/mysql';
import { PostgresAdapter } from '../../../core/database/postgres';

export enum GET_TYPE {
    GET = 'get',
    QUERY = 'query',
    QUERY_LIKE = 'query_like',
}

export class SuperCM<T> {

    private db: PostgresAdapter | MySqlAdapter = environment.getBd();

    constructor(public src: string) { }
    /**
     * @description Obtiene todos los registros de T
     * @returns T[]
     */
    public async get() {
        try {
console.log('GETSITA');

            const response = await this.db.query(QSuper.get(this.src));
            return response;
        } catch (error) { return new SqlException(error) }
    }

    /**
     * @description Obtiene todos los registros de T
     * @param id Identificador con el que se buscará un registro de T
     * @returns T || null
     */
    public async getById(id: number) {
        try {
            console.log('GETSITA');
            const response: any = await this.db.query(QSuper.getById(this.src, id));
            return response && response.length > 0 ? response[0] : null;
        } catch (error) { return new SqlException(error) }
    }


    /**
     * @description Obtiene todos los registros de T con base a los argumentos
     * @param params es un objeto de tipo {campo: valor}, cada campo es un parámetro de búsqueda
     * @returns T[]
     */
    public async getByQuery(params: any) {
        try {
            console.log('GETSITA');
            const response = await this.db.query(QSuper.getByQuery(this.src, params));
            console.log('Pasa', response);

            return response;
        } catch (error) { return new SqlException(error) }
    }

    /**
     * @description Obtiene todos los registros de T con base a los argumentos de consulta, se convierten
     * en un query que busca por coincidencias de tipo ( campo LIKE %valor% )
     * @param params es un objeto de tipo {campo: valor}, cada campo es un parámetro de búsqueda
     * @returns T[]
     */
    public async getByQueryLike(params: any) {
        try {
            console.log('GETSITA');
            const response = await this.db.query(QSuper.getByQueryLike(this.src, params));
            console.log('Pasa', response);
            return response;
        } catch (error) { return new SqlException(error) }
    }

    /**
     * @description Crea un objeto de tipo T
     * @param params es un objeto de tipo {campo: valor}, cada campo indica una columna, y el valor, es el valor
     * que se asignará a esa columna
     * @returns number indica el número de registros afectados
     */
    public async postOne(payload: T) {
        try {
            const response = await this.db.query(QSuper.postOne(this.src, payload));
            return response;
        } catch (error) { return new SqlException(error) }
    }

    /**
     * @description Actualiza un objeto de tipo T
     * @param id es el identificador de el registro T a actualizar
     * @param payload es un objeto {campo:valor} del registro con los valores a actualizar
     * @returns number indica el número de registros afectados
     */
    public async putOneById(id: number, payload: T) {
        try {
            const response = await this.db.query(QSuper.putOneById(this.src, id, payload));
            return response;
        } catch (error) { return new SqlException(error) }
    }

    /**
    * @description Eliminar un registro de tipo T
    * @param id es el identificador de el registro T a eliminar
    * @returns number indica el número de registros afectados
    */
    public async deleteById(id: number) {
        try {
            const response = await this.db.query(QSuper.deleteById(this.src, id));
            return response;
        } catch (error) { return new SqlException(error) }
    }

}