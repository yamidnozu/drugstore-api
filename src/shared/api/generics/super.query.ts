/**
 * Aquí contienen definiciones de consultas SQL genéricas que sonusadas en SUPERCM
 * Que es una clase que puede ser heredada y extender funcionalidades  a otros CM
 * Estas definiciones pueden aumentarse paulatimnamente
 */

export const QSuper = {
    get: (resource: string) => `SELECT * FROM ${resource}`,

    getById: (resource: string, id: number | string) =>
        `SELECT * FROM ${resource} WHERE id = ${id}`,

    getByQuery: (resource: string, params: string) =>
        `SELECT * FROM ${resource} WHERE ${getFieldValueWhere(params)}`,

    getByQueryLike: (resource: string, params: string) =>
        `SELECT * FROM ${resource} WHERE ${getFieldValueWhereLike(params)}`,

    postOne: (resource: string, payload: any) =>
        `INSERT INTO ${resource} ( ${(getFieldsOfPayload(payload))} ) VALUES ( ${getValuesOfPayload(payload)} )`,

    putOneById: (resource: string, id: number, payload: any) =>
        `UPDATE ${resource} SET ${(getFieldValueSet(payload))} WHERE id = ${id}`,

    deleteById: (resource: string, id: number) =>
        `DELETE FROM ${resource} WHERE id = ${id}`

}


const getFieldsOfArray = (array: string[]) => array.join(',');

const getFieldsOfPayload = (payload: any) => Object.keys(payload).join(',');

const getValuesOfPayload = (payload: any) => Object.values(payload).map(item => `'${item}'`).join(',');

const getFieldValueWhere = (params: any) => {
    const response: string[] = [];
    Object.keys(params).forEach(field => response.push(` ${field} = '${params[field]}'`));
    return response.join(' AND ');
};

const getFieldValueSet = (params: any) => {
    const response: string[] = [];
    Object.keys(params).forEach(field => response.push(` ${field} = '${params[field]}'`));
    return response.join(',');
};

const getFieldValueWhereLike = (params: any) => {
    const response: string[] = [];
    Object.keys(params).forEach(field => {
        if (!isNaN(params[field])) {
            response.push(` ${field} = ${params[field]}`);
        } else {
            response.push(` ${field} LIKE '%${params[field]}%'`);
        }
    });
    return response.join(' OR ');
};


// get // Obtiene todos los elementos de una tabla
// getById // Obtiene un único elemento por su id
// getByParams // Se pasarian un conjunto de parámetros para filtrar dentro de unamisma tabla
// getPaginate // Jajaja o como sea que se llame, la idea es para paginar desde un puinto inicial hasta un final

// Habrian más de tipo de consulta de múltiples tablas a través de JOINS, bueno pero esto hay que ver porque no tengo experiencia con esto de sql
// Por otra parte estan los POST
// postOne // Que agrege un solo elemento
// postMultiple // Que agregará múltiples elementos
// postOneOneToOne // Que sea para agregar registros de 1 a 1
// postMultipleOneToOne // Para agregar registros de 1 a 1 pero múltiples
// No se me ocurren por lo pronto más

// Por otra parte los PUT
// putOneById // Actualiza un registro segun su id
// putOneByParam // Actualza uno o más registros según multiples parametros
// // Las de actualización creo que no se necesitaria entre múltiples relaciones no? con el id
// // solamente podemos actualizar registros

// Para el delete seria
// deleteById // Elimina un registro por id
// deleteByParams // Elimina un registro por según sus diferentes parámetros
// deleteByRel // Elimina un registro según sus relaciones
// // No se que otros tipos habrian
// El hecho es tener funciones genéricas que son fácil de construir que ayudarpan a desarrollar api lo más sencillo posible solo parametrizandolas
// con unas herencias de clases espero que lo más sencillas, no se como ves
