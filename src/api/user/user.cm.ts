import { SuperCM } from '../../shared/api/generics/super.cm';
import { User } from './user.dto';

/**
 * En esta clase se  administra la información de los User
 * Aqui se debe manejar todo lo relacionado con el USER
 * con la BD
 * Para el ejemplo se extiende de SuperCM que contiene unas consultas ya implementadas
 */
export class UsersCM extends SuperCM<User> {
    constructor() { super('users') }
}