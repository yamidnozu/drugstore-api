import { NextFunction, Request, Response, Router } from 'express';
import Controller from '../../core/interfacess/controller.interface';
import { AuthService } from '../../core/Services/AuthService';
import { GET_TYPE } from '../../shared/api/generics/super.cm';
import { UsersCM } from './user.cm';
import { User } from './user.dto';




export default class UserController implements Controller {
    public path = '/users';
    public router = Router();
    public usersCm = new UsersCM();

    constructor() {
        this.intializeRoutes();
    }

    public intializeRoutes() {
        this.router.get(this.path + '', AuthService.guard(), this.getUsers);
        this.router.get(this.path + '/:id', AuthService.guard(), this.getUserById);
        this.router.post(this.path + '', AuthService.guard(), this.postUserOne);
        this.router.put(this.path + '/:id', AuthService.guard(), this.putUserOneById);
        this.router.delete(this.path + '/:id', AuthService.guard(), this.deleteUserById);
    }

    getUsers = async (req: Request, res: Response, next: NextFunction) => {
        const params = req.query;
        let response = null;
        if (params.type) {
            // Si llega parametro type
            const type = params['type'];
            delete params['type'];
            if (type === GET_TYPE.QUERY) {
                // Búsqueda por user?type=query&name=Yamid // where name = 'Yamid'
                response = await this.usersCm.getByQuery(params);
            } else {
                // Búsqueda por user?type=query_query&name=Yamid  // where name LIKE ('%Yamid%')
                response = await this.usersCm.getByQueryLike(params);
            }
        } else {
            // Si no llega parametro type
            response = await this.usersCm.get();
        }
        if (response instanceof Error) { return next(response) }
        return res.status(200).json(response);
    }

    getUserById = async (req: Request, res: Response, next: NextFunction) => {
        const id = req.params.id;
        const response = await this.usersCm.getById(+id);
        if (response instanceof Error) { return next(response) }
        return res.status(200).json(response);
    }

    postUserOne = async (req: Request, res: Response, next: NextFunction) => {
        const payload = req.body as User;
        const response = await this.usersCm.postOne(payload);
        if (response instanceof Error) { return next(response) }
        return res.status(200).json(response);
    }

    putUserOneById = async (req: Request, res: Response, next: NextFunction) => {
        const payload = req.body as User;
        const id = req.params['id'];
        const response = await this.usersCm.putOneById(+id, payload);
        if (response instanceof Error) { return next(response) }
        return res.status(200).json(response);
    }

    deleteUserById = async (req: Request, res: Response, next: NextFunction) => {
        const id = req.params['id'];
        const response = await this.usersCm.deleteById(+id);
        if (response instanceof Error) { return next(response) }
        return res.status(200).json(response);
    }

}