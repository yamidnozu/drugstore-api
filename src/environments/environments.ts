import { MySqlAdapter } from '../core/database/mysql';
import { PostgresAdapter } from '../core/database/postgres';
import dev from './cert/keycloack/dev';
import hulk from './cert/keycloack/hulk';
import iron from './cert/keycloack/iron';
import prod from './cert/keycloack/prod';
import test from './cert/keycloack/test';
export const environment = {
    version: '0.0.0',
    proyecto: 'drugstore.api',

    db: {
        active: process.env.BD || 'mysql' || 'postgres',
        mysql: {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'typescriptdatabase',
            connectionLimit: 10
        },
        postgres: {
            user: 'postgres',
            host: 'localhost',
            password: 'asd',
            database: 'typescriptdatabase',
            port: 5432
        }
    },
    dev: {
        access: dev,
        key: 'dev',
        port: 3000,
        environment: 'dev'
    },
    hulk: {
        access: hulk,
        key: 'hulk',
        port: 3000,
        environment: 'dev'
    },
    iron: {
        access: iron,
        key: 'iron',
        port: 3000,
        environment: 'dev'
    },
    prod: {
        access: prod,
        key: 'prod',
        port: 3000,
        environment: 'prod'
    },
    test: {
        access: test,
        key: 'test',
        port: 3000,
        environment: 'test'
    },
    produccion: {
        access: prod,
        key: 'Produccion',
        port: 3000,
        environment: 'prod'
    },
    getMode: (): any => {
        return process.env.ENV ? process.env.ENV : 'dev';
    },
    getEnv: () => {
        let env = environment.getMode();
        let modo = (environment as any)[env];
        return modo;
    },
    getTypeEnv: () => {
        return environment.getEnv().environment;

    },
    getCert: (): any => {
        return environment.getEnv().access;
    },
    getProject: (): string => {
        return environment.proyecto;
    },
    getVersion: (): string => {
        let env = environment.getMode();
        let modo = (environment as any)[env].key;
        return environment.version + '.' + modo;
    },
    getPort: (): number => {
        let env = environment.getMode();
        let port = (environment as any)[env].port;
        return port;
    },
    getBdConnection(): string {
        return environment.db.active;
    },
    getConfigBd: (): any => {
        return (environment.db as any)[environment.getBdConnection()];
    },
    getBd: () => {
        if (environment.db.active === 'postgres') {
            return new PostgresAdapter(environment.getConfigBd());
        }
        return new MySqlAdapter(environment.getConfigBd())
    }
}
