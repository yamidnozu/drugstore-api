export default {
    'realm': 'drugstore',
    'auth-server-url': 'http://35.238.54.102:8080/auth',
    'ssl-required': 'external',
    'resource': 'drugstore-api',
    'confidential-port': 0,
}
