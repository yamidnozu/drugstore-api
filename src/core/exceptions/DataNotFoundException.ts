import HttpException from "./HttpException";

class DataNotFoundException extends HttpException {
  constructor(codigo: string) {
    super(404, 'Información no encontrada', codigo);
  }
}

export default DataNotFoundException;