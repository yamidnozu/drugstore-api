import HttpException from "./HttpException";
 
class StandarException extends HttpException {
  constructor(mesage: string, cod: string, object?: Error) {
    super(400, mesage, cod, object);
  }
}
 
export default StandarException;