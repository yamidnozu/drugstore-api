import HttpException from "./HttpException";
 
class DataNotSaveException extends HttpException {
  constructor(codigo: string) {
    super(400, 'Información no almacenada', codigo);
  }
}
 
export default DataNotSaveException;