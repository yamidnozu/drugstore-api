import { ERROR_CODE } from "./codes";

/**
 * https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 */
class HttpException extends Error {
  status: number;
  msj: string;
  codigo: string;
  extra: any;

  constructor(
    status: number,
    msj: string,
    codigo: string = ERROR_CODE.INTEFINIDO,
    extra?: Error | any,
  ) {
    super(msj);
    this.status = status;
    this.msj = msj;
    this.codigo = codigo;
    if (extra !== undefined && extra !== null) {
      this.extra = {
        name: extra.name,
        message: extra.message
      }
    }
  }
}

export default HttpException;