import HttpException from "./HttpException";
import { ERROR_CODE } from "./codes";

class InternalServerException extends HttpException {
  constructor(
    mesage: any,
    object?: Error | any,
  ) {
    super(500, mesage, ERROR_CODE.INTEFINIDO, object);
  }
}

export default InternalServerException;