import HttpException from "./HttpException";
import { ERROR_CODE } from "./codes";

class SqlException extends HttpException {
  constructor(error?: any) {
    super(500, error, ERROR_CODE.SQL, error);
  }
}

export default SqlException;