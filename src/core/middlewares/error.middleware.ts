import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/HttpException.exception';
import { environment } from '../../environments/environments';


function errorMiddleware(error: HttpException, request: Request, response: Response, next: NextFunction) {
    const res = {
        estatus: "error",
        mensaje: environment.getTypeEnv() === 'dev' ? (error as any).msj : error.mensaje || 'Algo salio mal perro!',
        codigo: error.codigo
    } as any;
    if (error.extra !== undefined) {
        res.extra = error.extra;
    }
    response
        .status(error.status || 500)
        .send(res);
}

export default errorMiddleware;