import { Pool, QueryResult } from 'pg';
import { environment } from '../../environments/environments';

/**
 * Adaptador para mysql que retorna una promesa y tendrá compatibilidad de respuesta
 * con ostros adaptadores si se agregan, por ejemplo el de mysql
 */
export class PostgresAdapter {
    public connection: Pool;
    constructor(config: any) {
        this.connection = new Pool(config || environment.db.postgres);
    }
    query(sql: any, args?: any) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err: Error, rows: QueryResult) => {
                if (err)
                    return reject(err);
                resolve(rows.rows);
            });
        });
    }
    close() {
        return new Promise((resolve, reject) => {
            this.connection.end(() => {
                resolve();
            });
        });
    }
}