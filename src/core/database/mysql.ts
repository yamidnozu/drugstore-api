import mysql, { MysqlError } from 'mysql';

/**
 * Adaptador para mysql que retorna una promesa y tendrá compatibilidad de respuesta
 * con ostros adaptadores si se agregan, por ejemplo el de postgres
 */
export class MySqlAdapter {
    public connection: any;
    constructor(config: any) {
        this.connection = mysql.createConnection(config);
    }
    query(sql: any, args?: any): any {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err: any, rows: any) => {
                if (err)
                    return reject(err);
                resolve(rows);
            });
        });
    }
    close() {
        return new Promise((resolve, reject) => {
            this.connection.end((err: MysqlError) => {
                if (err)
                    return reject(err);
                resolve();
            });
        });
    }
}