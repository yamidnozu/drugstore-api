import session from 'express-session';
import Keycloak from 'keycloak-connect';



export class AuthService {
    private static memoryStore = new session.MemoryStore();
    public static keycloak: Keycloak.Keycloak;
    public static instance: AuthService | null = null;
    private constructor() { }
    public static getInstance() {
        return this.instance || new AuthService();
    }
    public static configureConectionWithKeycloak(app?: any, config?: any) {
        if (app && config) {
            this.keycloak = new Keycloak({ store: AuthService.memoryStore }, config);
            app.use(
                session({
                    secret: 'unloboazul',
                    resave: false,
                    saveUninitialized: true,
                    store: this.memoryStore
                })
            );
            app.use(this.keycloak.middleware())
        }
    }

    public static guard = () => AuthService.keycloak.protect(); // middleware()

}