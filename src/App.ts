import * as bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import Controller from './core/interfacess/controller.interface';
import errorMiddleware from './core/middlewares/error.middleware';
import { AuthService } from './core/Services/AuthService';



class App {
  private app: express.Application;

  constructor(controllers: Controller[], config?: any) {
    this.app = express();
    this.initAuthMiddleware(config);
    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  private initAuthMiddleware(config: any) {
    AuthService.getInstance();
    AuthService.configureConectionWithKeycloak(this.app, config);
  }

  private initializeMiddlewares() {
    this.app.use(bodyParser.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cors({ origin: true }));
    this.app.use(cookieParser());
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }

  public getApp() {
    return this.app;
  }
}

export default App;